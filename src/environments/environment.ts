// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :
  {
    apiKey: "AIzaSyAa0k8VbOda6x6lAuKVSiLnElAcamHAYRg",
    authDomain: "tsw-maps-241314.firebaseapp.com",
    databaseURL: "https://tsw-maps-241314.firebaseio.com",
    projectId: "tsw-maps-241314",
    storageBucket: "tsw-maps-241314.appspot.com",
    messagingSenderId: "134847809916",
    appId: "1:134847809916:web:f24b67eb533e0be2"
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
