import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router'
import { HomeComponent } from './components/home/home.component';
// import { MapsAdminComponent } from './components/Admin/maps-admin/maps-admin.component';
import { LoginComponent } from './components/login/login.component';
// import { RegistroComponent } from './components/Admin/registro/registro.component';
import { VistaAdminComponent } from './components/Admin/vista-admin/vista-admin.component';
import { MapsComponent } from './components/User/maps/maps.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'VistaAdmin', component: VistaAdminComponent,canActivate:[AuthGuard]},
  { path: 'Home', component:HomeComponent},
  { path: 'User', component:MapsComponent,canActivate:[AuthGuard]}

]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }
