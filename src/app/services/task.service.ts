import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Task } from './../interfaces/task';
@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
   private http: HttpClient
  ) { }
  
  getAllTasks() {
    const path = 'https://consultas.axuretechnologies.com:8443/cxf/axure_tsw/estado/axure';
    return this.http.get<Task[]>(path);
  }
  getTask(id: string) {
    const path = `https://consultas.axuretechnologies.com:8443/cxf/axure_tsw/estado/${id}`;
    return this.http.get<Task[]>(path);
  }
}
