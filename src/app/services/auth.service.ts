import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from "angularfire2/firestore";
import { map } from 'rxjs/operators';
// import { auth } from 'firebase/app';
import { promise } from 'protractor';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afsAuth: AngularFireAuth, private db: AngularFirestore ) { }

registrarUsuario(email: string, pass:string,Empresa:string,Nombre_Usuario:string)
{
 return new Promise((resolve, reject ) => {
   this.afsAuth.auth.createUserWithEmailAndPassword(email,pass)
   .then((success) => {
    var user = firebase.auth().currentUser;
    user.updateProfile({
      displayName: "User"
    });
    this.db.collection('User').doc(user.uid).set({
      Nombre:Nombre_Usuario,
      Correo: user.email,
      Empresa:Empresa,
      Rol: "User"
    })
    
  });
 })
}
LoginUser(email:string, pass: string)
{
  return new Promise((resolve, reject) => {
    this.afsAuth.auth.signInWithEmailAndPassword(email, pass)
    .then( userData => resolve(userData),
    err => reject(err));
  });

}
LogOutUser()
{
  return this.afsAuth.auth.signOut();
 
}

isAuth()
{
  return  this.afsAuth.authState.pipe(map( auth => auth));
}



}
