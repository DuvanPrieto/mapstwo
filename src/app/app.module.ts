import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'
import { AppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';
import { LoginComponent } from './components/login/login.component';
import { MapsComponent } from './components/User/maps/maps.component';
import { RegistroComponent } from './components/Admin/registro/registro.component';
import { MapsAdminComponent } from './components/Admin/maps-admin/maps-admin.component';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { from } from 'rxjs';
import { NavbarComponent } from './components/navbar/navbar.component';
import { environment } from '../environments/environment';
import { VistaAdminComponent } from './components/Admin/vista-admin/vista-admin.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreModule } from "angularfire2/firestore";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MapsComponent,
    RegistroComponent,
    MapsAdminComponent,
    HomeComponent,
    NavbarComponent,
    VistaAdminComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAFzXNo0e1LOVohiQay1Wy0UHZZYcaaEyc'
    }), AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule
    
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  
})
export class AppModule { }
