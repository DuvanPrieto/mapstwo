import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../../services/task.service';
import { AuthService } from '../../../services/auth.service';
import * as $ from 'jquery';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from "angularfire2/firestore";
import * as firebase from 'firebase';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  lat: number = 4.7273085;
  lng: number = -73.5365581;
  user = firebase.auth().currentUser
  arreglo2 =[];
  
  
  constructor(private taskService:TaskService,private authService: AuthService, private db: AngularFirestore) {
    
      var docRef = db.collection("User").doc(this.user.uid);
      let EmpresaUser
      docRef.get().toPromise().then(function(doc) {
        if (doc.exists) {
            
            EmpresaUser = doc.data().Empresa;
            console.log(EmpresaUser);
           
        } else {
            
            console.log("No such document!");
        }
        }).catch(function(error) {
        console.log("Error getting document:", error);
      });

      // Ejecuta una vez 


      setTimeout(() => {
      this.taskService.getTask(EmpresaUser)
      .subscribe(todo => {
        this.arreglo2 = todo;
        if(this.arreglo2.length == 0)
        {
          alert("Error de conexión");
        }

      });
     }, 2000)


       
        // Ejecuta cada 8 segundos
        setInterval(() => {
          this.taskService.getTask(EmpresaUser)
          .subscribe(todo => {
            this.arreglo2 = todo;
            if(this.arreglo2.length == 0)
            {
                alert("Error de conexión");
            }
          });
        }, 8000)
      

   }
 
  
  
  ngOnInit() {
    
    $(".NavAdminBar").click(function(){
      if($(".contenedoritemsBarAdmin").hasClass('ActivarBarAdmin'))
      {
        $(".contenedoritemsBarAdmin").removeClass('ActivarBarAdmin')
      }
      else
      {
        $(".contenedoritemsBarAdmin").addClass('ActivarBarAdmin')
      }
});
    
    var openBtn = true;
      $(".itemNavBarAdmin3").click(function(){
          if(openBtn!)
          {
            $(".contenedorConvenciones").addClass('openBtn');  
          }
          else
          {
           
            $(".contenedorConvenciones").removeClass('openBtn');
          }
          openBtn = !openBtn;
      })
    
  }
  OnLogout()
  { 
    
    this.authService.LogOutUser();
    location.href ="/";
  }
}
