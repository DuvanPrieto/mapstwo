import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(private AuthService: AuthService,  private afsAuth: AngularFireAuth,private router: Router) { }
    public Logged: boolean = false; 
  ngOnInit() {
    this.getCurrentUser();


  }
  getCurrentUser()
  {
       this.AuthService.isAuth().subscribe( auth => {
          if( auth )
          {
           
            this.Logged = true;
          }
          else
          {
           
            this.Logged = false;
          }
       });
  }

  OnLogout()
  { 
    
    this.AuthService.LogOutUser();
    location.href ="/login";
  }

}
