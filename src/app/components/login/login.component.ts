import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth,private router: Router,private authService: AuthService ) 
  {

   }
 email: string = '';
 pass: string = '';
error: string = ''

  ngOnInit() {
  }
  onLogin()
  {
    console.log('email:', this.email);
    console.log( 'pass:' , this.pass);
    this.authService.LoginUser(this.email, this.pass)
    .then( (res) => {
      this.afAuth.authState.subscribe(authState => {
        if (authState) {
          console.log(authState.displayName);
          console.log(authState.email);
          if (authState.displayName == "User") {
            location.href = "/User";
            
          } else if (authState.displayName == "Admin") {
            location.href = "/VistaAdmin";
           
          }
        }
      })
      
    }).catch( (err) => {
      
      
      console.log('err',err.message)
      let ErrorLogin:string = err.message;
      // alert(ErrorLogin);
        if(ErrorLogin == "The email address is badly formatted.")
        {
          this.error = "La direccion de correo electronico es incorrecta."; 
        }
        else
        {
          this.error = "La contraseña no es válida o el usuario no existe.";  
        }
         $(".errorLogin").addClass('mostrarError'); 

        setTimeout(() => {

          $(".errorLogin").removeClass('mostrarError'); 

       }, 6000);
      
  });
  }
}
