import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-vista-admin',
  templateUrl: './vista-admin.component.html',
  styleUrls: ['./vista-admin.component.css']
})
export class VistaAdminComponent implements OnInit {

  constructor(private authService: AuthService) { }
  menuVista = "Maps";
  m = 1;
  ngOnInit() {

      $(".NavAdminBar").click(function(){
            if($(".contenedoritemsBarAdmin").hasClass('ActivarBarAdmin'))
            {
              $(".contenedoritemsBarAdmin").removeClass('ActivarBarAdmin')
            }
            else
            {
              $(".contenedoritemsBarAdmin").addClass('ActivarBarAdmin')
            }
      });

      var openBtn = true;
      $(".itemNavBarAdmin3").click(function(){
          if(openBtn!)
          {
            $(".contenedorConvenciones").addClass('openBtn');  
          }
          else
          {
           
            $(".contenedorConvenciones").removeClass('openBtn');
          }
          openBtn = !openBtn;
      })
  }
  menu(e) {
    if (e == 2) {
        this.menuVista = "Maps";
    } else if (e == 1) {
        this.menuVista = "Registro";
        
    }
    this.m = e;
  }
  OnLogout()
  { 
    
    this.authService.LogOutUser();
    location.href ="/";
  }
}
