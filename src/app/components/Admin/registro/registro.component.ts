import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { AngularFirestore } from "angularfire2/firestore";
import * as $ from 'jquery';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  constructor(private router: Router,private authService: AuthService, private db: AngularFirestore) { }
  public email:string = ''; 
  public pass:string = '';
  public Empresa:string = '';
  public Nombre_Usuario:string = '';
  ngOnInit() {
  }
  onAgregarUser()
  { 
    this.authService.registrarUsuario(this.email,this.pass,this.Empresa,this.Nombre_Usuario)
    .then((res) => {

    
      
      
      }).catch(err => {
        $(".MostrarAlertafallo").fadeIn(1000);
        setTimeout(() => {
          $(".MostrarAlertafallo").fadeOut(1000);
        }, 4000)
      });

      $(".MostrarAlertaCorrecto").fadeIn(1000);
      setTimeout(() => {
        $(".MostrarAlertaCorrecto").fadeOut(1000);
      }, 4000)

      $("#Nombre_Usuario").val('');
      $("#email").val('');
      $("#Empresa").val('');
      $("#pass").val('');
  }
}

